<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <html>
            <body>
                <table style="border: 1px solid black">
                    <tr>
                        <th style="border: 1px solid black; text-align:left">Bandera</th>
                        <th style="border: 1px solid black; text-align:left">Pais</th>
                        <th style="border: 1px solid black; text-align:left">Gobierno</th>
                        <th style="border: 1px solid black; text-align:left">Capital</th>

                    </tr>

                    <xsl:apply-templates select="mundo/continente/paises"/>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="mundo/continente/paises">

        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order="descending"/>

            
            <xsl:choose>
                <xsl:when test="continente=Africa">
                <tr>
                    <th style="border: 1px solid green; text-align:left"><xsl:value-of select="foto"></xsl:value-of></th>

                    <th style="border: 1px solid green; text-align:left"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid green; text-align:left"><xsl:value-of select="capital"></xsl:value-of></th>
                    <xsl:choose>
                        <xsl:when test="nombre/@gobierno=dictadura">
                            <th style="border: 1px solid green; background-color:red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:when test="@gobierno=monarquia">
                            <th style="border: 1px solid green; background-color:yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th style="border: 1px solid green; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>     

                </xsl:when>
                <xsl:when test="continente=Europe">
                <tr>
                    <th style="border: 1px solid gray; text-align:left"><xsl:value-of select="foto"></xsl:value-of></th>

                    <th style="border: 1px solid gray; text-align:left"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid gray; text-align:left"><xsl:value-of select="capital"></xsl:value-of></th>
                    <xsl:choose>
                        <xsl:when test="nombre/@gobierno=dictadura">
                            <th style="border: 1px solid gray; background-color:red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:when test="@gobierno=monarquia">
                            <th style="border: 1px solid gray; background-color:yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th style="border: 1px solid gray; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>     
                </xsl:when>
                <xsl:when test="continente=Oceanía">
                <tr>
                    <th style="border: 1px solid blue; text-align:left"><xsl:value-of select="foto"></xsl:value-of></th>

                    <th style="border: 1px solid blue; text-align:left"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid blue; text-align:left"><xsl:value-of select="capital"></xsl:value-of></th>
                    <xsl:choose>
                        <xsl:when test="nombre/@gobierno=dictadura">
                            <th style="border: 1px solid blue; background-color:red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:when test="@gobierno=monarquia">
                            <th style="border: 1px solid blue; background-color:yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th style="border: 1px solid blue; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>
                </xsl:when>
                <xsl:when test="continente=Asia">
                <tr>
                    <th style="border: 1px solid yellow; text-align:left"><xsl:value-of select="foto"></xsl:value-of></th>

                    <th style="border: 1px solid yellow; text-align:left"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid yellow; text-align:left"><xsl:value-of select="capital"></xsl:value-of></th>
                    <xsl:choose>
                        <xsl:when test="nombre/@gobierno=dictadura">
                            <th style="border: 1px solid yellow; background-color:red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:when test="@gobierno=monarquia">
                            <th style="border: 1px solid yellow; background-color:yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th style="border: 1px solid yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>     
                         
                </xsl:when>
                <xsl:otherwise>
                <tr>
                    <th style="border: 1px solid red; text-align:left"><xsl:value-of select="foto"></xsl:value-of></th>

                    <th style="border: 1px solid red; text-align:left"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid red; text-align:left"><xsl:value-of select="capital"></xsl:value-of></th>
                    <xsl:choose>
                        <xsl:when test="nombre/@gobierno=dictadura">
                            <th style="border: 1px solid red; background-color:red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:when test="@gobierno=monarquia">
                            <th style="border: 1px solid red; background-color:yellow; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th style="border: 1px solid red; text-align:left"><xsl:value-of select="nombre/@gobierno"></xsl:value-of></th>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>     
                         
                </xsl:otherwise>
            </xsl:choose>

        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>