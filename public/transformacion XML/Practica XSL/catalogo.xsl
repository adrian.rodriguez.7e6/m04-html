<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template match="/">
  
  
  <h1>Videojuegos</h1>

  <xsl:element name="div">
    <xsl:attribute name="class">Videojuegos</xsl:attribute>
    
    
    <xsl:for-each select="Videojuegos/Videojuego">
      <xsl:element name="article">
        <xsl:element name="div">
          <xsl:attribute name="class">Cajon</xsl:attribute>
            <xsl:element name="img">
            <xsl:attribute name="src">
              <xsl:value-of select="InfoGeneral/Foto"/>
            </xsl:attribute>
            </xsl:element>

            <xsl:element name="h3">
              <xsl:value-of select="Nombre"/>
            </xsl:element>

          <xsl:element name="div">
          <xsl:attribute name="class">Texto</xsl:attribute>
            <xsl:element name="p">
              <xsl:value-of select="InfoGeneral/Descripcion"/>
            </xsl:element>
          </xsl:element>

          <xsl:element name="div">
          <xsl:attribute name="class">Ver_mas</xsl:attribute>
            <xsl:element name="a">
              <xsl:attribute name="href">Detalle.html?id=<xsl:value-of select="@idVideojuego"/></xsl:attribute>
              Ver mas
            </xsl:element>
          </xsl:element>

        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:element>

  

</xsl:template>

</xsl:stylesheet>