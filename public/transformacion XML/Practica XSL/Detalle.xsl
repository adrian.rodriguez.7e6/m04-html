<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template match="/">

  <xsl:element name="section">
    <xsl:attribute name="class">Detalle</xsl:attribute>
    <xsl:element name="article">
        <xsl:attribute name="class">InfoPrincipal</xsl:attribute>
        <xsl:element name="h1"><xsl:value-of select="Videojuego/Nombre"/></xsl:element>
        <xsl:element name="img"><xsl:attribute name='src'><xsl:value-of select="Videojuego/InfoGeneral/Foto"/></xsl:attribute></xsl:element>
        <xsl:element name="p"><xsl:value-of select="Videojuego/InfoGeneral/Descripcion"/></xsl:element>
    </xsl:element>
    
    <xsl:element name="article">
        <xsl:element name="div">
        
        <xsl:attribute name="class">InfoSecundaria</xsl:attribute>
        <xsl:element name="p"><b>Genero:</b><xsl:value-of select="Videojuego/Genero"/><br/></xsl:element>
        <xsl:element name="p"><b>Desarrollador:</b><xsl:value-of select="Videojuego/InfoGeneral/Desarrollador"/><br/></xsl:element>
        <xsl:element name="p"><b>Distribuidor:</b><xsl:value-of select="Videojuego/InfoGeneral/Distribuidor"/><br/></xsl:element>
        <h3><b>Modos de juego:</b></h3>
        <xsl:for-each select="Videojuego/ModosJuego/ModoJuego">
          <xsl:element name="p"><xsl:value-of select="."/></xsl:element>
        </xsl:for-each>
        <h3><b>Plataformas:</b></h3>
        <xsl:for-each select="Videojuego/Plataformas/Plataforma">
          <xsl:element name="p"><xsl:value-of select="."/></xsl:element>
          
        </xsl:for-each>

        </xsl:element>
    </xsl:element>
  </xsl:element>

</xsl:template>

</xsl:stylesheet>