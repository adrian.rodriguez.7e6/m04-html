<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1" />

    <xsl:template match="/">
        <lista>
            <xsl:apply-templates></xsl:apply-templates>
        </lista>
    </xsl:template>

    <xsl:template match="discos/group">
    <xsl:variable name="nombreGrupo"><xsl:value-of select="name" /></xsl:variable>
    <xsl:variable name="idGrupo"><xsl:value-of select="@id" /></xsl:variable>
        
        <xsl:for-each select="../disco[interpreter/@id = $idGrupo]">
                <disco>
                    <xsl:value-of select="title"/>
                    interpretado por
                    <xsl:value-of select="$nombreGrupo"/>
                    
                </disco>

            </xsl:for-each>
    </xsl:template>

    <xsl:template match="discos/disco"></xsl:template>
</xsl:stylesheet>