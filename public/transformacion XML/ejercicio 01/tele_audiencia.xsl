<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="programacio">
    <html>
    <body>
        <nom>Un TV</nom>
        <programas>
            <xsl:apply templates/>
        </programas
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>

<xsl:template match="/audiencia"
    <programa>
        <xsl:attribute name="hora">
            <xsl:value-of select="hora"/>
        </xsl:attribute>