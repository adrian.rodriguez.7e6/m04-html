<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
    <html>
    <body>
        <table style="border: 1px solid black">
            <tr style="background-color:green">
                <th style="text-align:left">Foto</th>
                <th style="text-align:left">Nombre</th>
                <th style="text-align:left">Apellidos</th>
                <th style="text-align:left">Telefono</th>
                <th style="text-align:left">Repetidor</th>
                <th style="text-align:left">Nota Practica</th>
                <th style="text-align:left">Nota Examen</th>
                <th style="text-align:left">Nota Total</th>
            </tr>
            <xsl:for-each select="evaluacion/alumno">
            <xsl:sort select="apellidos" order="ascending"/>
            <tr>
                <img>
                    <xsl:choose>
                        <xsl:when test="imagen">
                            <xsl:value-of select="imagen"></xsl:value-of>
                        </xsl:when>
                        
                        <xsl:otherwise>
                            <xsl:attribute name="src">public/transformacion XML/ejercicio 01/Fotos evaluacion/Default.png</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </img>

                
                
                
                <th><xsl:value-of select="nombre"></xsl:value-of></th>
                <th><xsl:value-of select="apellidos"></xsl:value-of></th>
                <th><xsl:value-of select="telefono"></xsl:value-of></th>
                <th><xsl:value-of select="@repite"></xsl:value-of></th>
                <th><xsl:value-of select="notas/practicas"></xsl:value-of></th>
                <th><xsl:value-of select="notas/examen"></xsl:value-of></th>
                <xsl:if test="(notas/practicas+notas/examen)*0.5 &lt;=4.9">
                <th style="background-color:red"><xsl:value-of select="(notas/practicas+notas/examen)*0.5"></xsl:value-of></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)*0.5 &gt;=8">
                <th style="background-color:blue"><xsl:value-of select="(notas/practicas+notas/examen)*0.5"></xsl:value-of></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)*0.5 &lt;=8 and (notas/practicas+notas/examen)*0.5 &gt;=4.9">
                <th><xsl:value-of select="(notas/practicas+notas/examen)*0.5"></xsl:value-of></th>
                </xsl:if>
            </tr>
            </xsl:for-each>
        </table>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>