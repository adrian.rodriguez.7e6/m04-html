<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
    <html>
    <body>
        <table style="border: 1px solid black">
            <tr>
                <th style="border: 1px solid black; text-align:left">Fecha</th>
                <th style="border: 1px solid black; text-align:left">Máxima</th>
                <th style="border: 1px solid black; text-align:left">Mínima</th>
                <th style="border: 1px solid black; text-align:left">Predicción</th>

            </tr>
        
            <xsl:apply-templates select="root/prediccion"/>
        </table>
    </body>
    </html>
    </xsl:template>
    <xsl:template match="root/prediccion">
            
            <xsl:for-each select="dia">
            <xsl:sort select="temperatura/maxima" order="descending"/>
            <tr>
                <th style="border: 1px solid black; text-align:left"><xsl:value-of select="@fecha"></xsl:value-of></th>
                <th style="border: 1px solid black; text-align:left"><xsl:value-of select="temperatura/maxima"></xsl:value-of></th>
                <th style="border: 1px solid black; text-align:left"><xsl:value-of select="temperatura/minima"></xsl:value-of></th>
                <th style="border: 1px solid black; text-align:left"><img src="{concat('Fotos Aemet/',estado_cielo[@periodo='00-24']/@descripcion)}.png"/></th>
            </tr>
            </xsl:for-each>
        
    </xsl:template>

</xsl:stylesheet>
